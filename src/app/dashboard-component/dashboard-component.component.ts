import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackendServiceService } from '../_services/backend-service.service';
import * as CanvasJS from '../../assets/js/canvasjs.min';

@Component({
  selector: 'app-dashboard-component',
  templateUrl: './dashboard-component.component.html',
  styleUrls: ['./dashboard-component.component.css']
})
export class DashboardComponentComponent implements OnInit {

  constructor(private fb: FormBuilder, private bs: BackendServiceService) { }

  Students: any;
  DataPoints: any;
  Courses: any;
  coursesArray = [];

  ngOnInit(): void {
    this.getStudents();
  }

  getStudents() {
    this.bs.getDetails().subscribe(data => {
      this.Students = data;
      // console.log(data);
      this.getCourses();
    }, error => {
      console.log(error);

    });
  }

  getCourses() {
    this.bs.getCourses().subscribe(data => {
      this.Courses = data;
      // console.log(data);
      this.processData();
    }, error => {
      console.log(error);
    })
  }

  processData() {
    for (let i = 0; i < this.Courses.length; i++) { //loop through courses and format them accordingly
      let coursename = this.Courses[i].course_name;
      let courseobj = { y: 0, label: coursename };
      this.coursesArray.push(courseobj);
    }

    for (let p = 0; p < this.Students.length; p++) { //loop through students and increment value for each course
      let studentcourse = this.Students[p].course;
      this.incrementCourse(studentcourse);
    }

    let chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: ""
      },
      data: [{
        type: "column",
        dataPoints: this.coursesArray
      }]
    });
    chart.render();
  }


  incrementCourse(course) {
    for (let i = 0; i < this.coursesArray.length; i++) {
      if (this.coursesArray[i].label == course) {
        this.coursesArray[i].y = this.coursesArray[i].y + 1;
      }
    }
  }
}
