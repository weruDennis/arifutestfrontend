import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackendServiceService } from '../_services/backend-service.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private fb: FormBuilder, private bs: BackendServiceService) { }

  UserDetailsForm: FormGroup;
  InvalidForm = false;
  Submitted = false;
  Courses: any;

  ngOnInit(): void {
    this.UserDetailsForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      course: ['', Validators.required],
      postal_address: ['', Validators.required],
      email: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"), Validators.required]],
    });
    this.getCourses();
  }

  getCourses() {
    this.bs.getCourses().subscribe(data => {
      this.Courses = data;
    }, error => {
      console.log(error);
    })
  }

  submitDetails() {
    this.Submitted = true;
    if (this.UserDetailsForm.invalid) {
      this.InvalidForm = true;
      return;
    }
    this.bs.postDetails(this.UserDetailsForm.value).subscribe(data => {
      this.InvalidForm = false;
      this.Submitted = false;
      if (data == 1) {
        this.showNotification();
        this.UserDetailsForm.reset();
      }
    }, error => {
      this.Submitted = false;
      this.InvalidForm = false;
      console.log(error);
    });
  }

  showNotification() {
    document.getElementById('successbtn').click();
  }
}
