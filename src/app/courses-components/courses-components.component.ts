import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { BackendServiceService } from '../_services/backend-service.service';

@Component({
  selector: 'app-courses-components',
  templateUrl: './courses-components.component.html',
  styleUrls: ['./courses-components.component.css']
})
export class CoursesComponentsComponent implements OnInit {

  constructor(private fb: FormBuilder, private bs: BackendServiceService) { }

  CoursesForm: FormGroup;
  Courses: any;
  InvalidForm = false;
  Submitted = false;

  ngOnInit(): void {
    this.CoursesForm = this.fb.group({
      course_name: ['', Validators.required]
    });
    this.getCourses();
  }

  getCourses() {
    this.bs.getCourses().subscribe(data => {
      this.Courses = data;
    }, error => {
      console.log(error);
    })
  }

  submitDetails() {
    this.Submitted = true;
    if (this.CoursesForm.invalid) {
      this.InvalidForm = true;
      return;
    }
    this.bs.postCourse(this.CoursesForm.value).subscribe(data => {
      this.InvalidForm = false;
      this.Submitted = false;
      if (data == 1) {
        this.showNotification();
        this.getCourses();
        this.CoursesForm.reset();
      }
    }, error => {
      this.Submitted = false;
      this.InvalidForm = false;
      console.log(error);
    });
  }

  showNotification() {
    document.getElementById('successbtn').click();
  }
}
