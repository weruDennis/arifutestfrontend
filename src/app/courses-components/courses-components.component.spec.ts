import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesComponentsComponent } from './courses-components.component';

describe('CoursesComponentsComponent', () => {
  let component: CoursesComponentsComponent;
  let fixture: ComponentFixture<CoursesComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
