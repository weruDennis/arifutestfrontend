import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesComponentsComponent } from './courses-components/courses-components.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { HomepageComponent } from './homepage/homepage.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponentComponent },
  { path: 'courses', component: CoursesComponentsComponent },
  { path: '', component: HomepageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
