import { Injectable } from '@angular/core';
import { config } from '../_helpers/global';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendServiceService {

  constructor(private http: HttpClient) { }

  postDetails(data) {
    return this.http.post<any>(`${config.apiUrl}/post/details`, data);
  }

  getDetails() {
    return this.http.get(`${config.apiUrl}/get/details`);
  }

  postCourse(data) {
    return this.http.post<any>(`${config.apiUrl}/post/course`, data);
  }

  getCourses() {
    return this.http.get(`${config.apiUrl}/get/courses`);
  }
}
