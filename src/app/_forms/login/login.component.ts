import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { LoggedInService } from '../../_services/logged-in-service.service';
import { config } from '../../_helpers/global';

@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LoginForm : FormGroup;
  loading = false;
  unregisterederror = false;
  loginerror = false;
  loggedin = false;
  admin = false;
  submitted = false;
  accesslevel:any;
  username:any;
  checkStatus:any;

  constructor(private loggedInService : LoggedInService, private fb: FormBuilder) {
    this.checkStatus = this.loggedInService.checkStatus();
    // this.loggedin = this.loggedInService.loggedin;
    this.admin = this.loggedInService.admin;
    this.accesslevel = this.loggedInService.accesslevel;
    this.username = this.loggedInService.username;
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.LoginForm = this.fb.group({
      email: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"), Validators.required]],
      password: ['', Validators.required]
    });
  }

  login() {
    this.submitted = true;
    if (this.LoginForm.invalid) {
      return;
    }
    this.loading=true;
    var self = this;
    console.log('loging in');
    $.when(
      $.ajax(`${config.apiUrl}/login`,
    {
      type:'POST',
      data: this.LoginForm.value, 
      success: function (success) {   // success callback function
        console.log(success);
     },
      error: function (errorMessage) { // error callback 
        //console.log(errorMessage);
      }
    })
    ).done(function(success){
      self.loading = false;
      self.submitted = false;
      if (success == 912) { //user does not exist
          self.unregisterederror = true; 
          self.loginerror = false;
      } else if (success == 401) { //something went wrong
          self.loginerror = true;
          self.unregisterederror = false;
      } else { //success
        self.unregisterederror = false;
        self.loginerror = false;
        self.loggedin = true;
        setTimeout(function(){
          $('#id01').fadeOut();
        }, 1200);
        var user = success['success'];
        self.username = user['name'];
        var admin = user['access_level'];
        if (admin == 1) {
          self.admin = true;
        }
        const now = new Date();
        const freedom_user = {
          user : user,
          expiry: now.getTime() + 86400000 //add one day to current time
        }
        localStorage.setItem('dashboard', JSON.stringify(freedom_user));
        // window.location.reload();
        // console.log(JSON.parse(localStorage.getItem('user')));
        //console.log(success);
      }
    });
  }

  toggleRegister () {
    $('#id01').fadeOut();
    $('#id02').fadeIn();
  }

  toggleLogin () {
    $('#id02').css({'display': 'none'});
    $('#id01').css({'display': 'block'});
  }
}
