import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './_forms/register/register.component';
import { HeadersComponent } from './_elements/headers/headers.component';
import { LoggedInService } from 'src/app/_services/logged-in-service.service';
import { LoginComponent } from './_forms/login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { CoursesComponentsComponent } from './courses-components/courses-components.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeadersComponent,
    HomepageComponent,
    DashboardComponentComponent,
    CoursesComponentsComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [LoggedInService],
  bootstrap: [AppComponent]
})
export class AppModule { }
